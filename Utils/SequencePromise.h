#pragma once

#include "SubjectBase.h"
#include "ObservableSequence.h"

template <typename T, typename E = void>
class SequencePromise : public SubjectBase<T, E>
{
    using Base = SubjectBase<T, E>;

public:
    using Observable = ObservableSequence<T, E>;

    explicit SequencePromise()
        : Base(Base::Type::Sequence)
    {}

    Observable observable() const
    {
        return Observable(this->subscriber());
    }

    void setNext(const T& value)
    {
        this->sync(QVariant::fromValue<T>(value), Base::SyncReason::ByValue);
    }

    void setComplete()
    {
        this->sync({}, Base::SyncReason::ByComplete);
    }
};


template <typename E>
class SequencePromise<void, E> : public SubjectBase<void, E>
{
    using Base = SubjectBase<void, E>;

public:
    using Observable = ObservableSequence<void, E>;

    explicit SequencePromise()
        : Base(Base::Type::Sequence)
    {}

    Observable observable() const
    {
        return Observable(this->subscriber());
    }

    void setNext()
    {
        this->sync({}, Base::SyncReason::ByValue);
    }

    void setComplete()
    {
        this->sync({}, Base::SyncReason::ByComplete);
    }
};
