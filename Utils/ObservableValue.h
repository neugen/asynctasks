#pragma once

#include "ObservableImpl.h"

template <typename T, typename E>
class ObservableValue : private ObservableImpl<T, E>
{
    using Impl = ObservableImpl<T, E>;
    using Subscriber = SubscriptionProvider<T, E>;
    using ValueCallbackType = typename Subscriber::ValueCallbackType;
    using ErrorCallbackType = typename Subscriber::ErrorCallbackType;

public:
    explicit ObservableValue(std::weak_ptr<Subscriber>&& ref)
        : Impl(std::move(ref))
    {}

    class Builder
    {
        Impl& m_impl;

    public:
        explicit Builder(Impl& impl)
            : m_impl(impl)
        {}

        ~Builder()
        {
            m_impl.subscribe();
        }

        void onError(ErrorCallbackType&& callback)
        {
            m_impl.m_params.callbacks->errorCallback = std::move(callback);
        }
    };

    Builder onComplete(QObject* context, ValueCallbackType&& callback)
    {
        Q_ASSERT(!this->m_subscribed && "already subscribed");

        this->m_params = typename Subscriber::Params(
                    context, std::move(callback));

        return Builder(*this);
    }

    bool isSubscribed() const
    {
        return this->m_subscribed;
    }
};
