#pragma once

#include <memory>

#include <QThread>
#include <QAbstractEventDispatcher>

#include "Traits.h"

template <typename T, typename E>
struct SubscriptionProvider
{
    using ValueCallbackType = typename Function<T>::type;
    using ErrorCallbackType = typename Function<E>::type;

    struct Params
    {
        struct Callbacks
        {
            ValueCallbackType valueCallback;
            std::function<void()> completeCallback;
            ErrorCallbackType errorCallback;
        };

        QObject* context = nullptr;
        std::shared_ptr<Callbacks> callbacks = std::make_shared<Callbacks>();

        Params() = default;

        Params(QObject* context, ValueCallbackType&& valueCallback)
            : context(context)
        {
            Q_ASSERT(context && "context was not set");

            QThread* thread = context->thread();
            Q_ASSERT(thread && thread->eventDispatcher() && "context has no event loop");

            callbacks->valueCallback = std::move(valueCallback);
        }
    };

    std::function<void(Params&&)> subscribe;

    explicit SubscriptionProvider() = default;

    explicit SubscriptionProvider(std::function<void(Params&&)>&& callback)
        : subscribe(std::move(callback))
    {}
};
