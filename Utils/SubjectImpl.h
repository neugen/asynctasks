#pragma once

#include <QMetaObject>

#include "SubscriptionProvider.h"

template <typename T, typename E>
class SubjectImpl
{
    using Subscriber = SubscriptionProvider<T, E>;
    using SubscriberPtr = std::shared_ptr<Subscriber>;
    using ParamsType = typename Subscriber::Params;
    using CallbacksType = typename ParamsType::Callbacks;

    int c_type;
    bool m_valid = false;
    ParamsType m_resultParams;
    SubscriberPtr m_subscriber;

protected:
    enum Type
    {
        Single, Sequence
    };

    enum class SyncReason
    {
        None, ByValue, ByError, ByComplete
    };

    SubjectImpl(const SubjectImpl&) = delete;
    SubjectImpl& operator=(const SubjectImpl&) = delete;

    SubjectImpl(SubjectImpl&&) = default;
    SubjectImpl& operator=(SubjectImpl&&) = default;

    explicit SubjectImpl(Type type)
        : c_type(type)
    {
        m_subscriber = std::make_shared<Subscriber>(
                    [this](ParamsType&& params)
        {
            m_resultParams = std::move(params);
            m_subscriber.reset();
            m_valid = true;
        });
    }

    ~SubjectImpl()
    {
        if (m_valid && m_resultParams.callbacks->completeCallback)
            sync({}, SyncReason::ByComplete);
    }

    bool isValid() const
    {
        return m_valid;
    }

protected:
    std::weak_ptr<Subscriber> subscriber() const
    {
        Q_ASSERT(m_subscriber && "already initialized");
        return std::weak_ptr<Subscriber>(m_subscriber);
    }

    void sync(QVariant value, SyncReason reason)
    {
        if (!m_valid)
            return;

        if (!(c_type == Type::Sequence && reason == SyncReason::ByValue))
            m_valid = false;

        std::shared_ptr<CallbacksType> callbacks = m_resultParams.callbacks;

        QMetaObject::invokeMethod(
                    m_resultParams.context, [value, reason, callbacks]()
        {
            switch (reason)
            {
            case SyncReason::ByValue:
                Function<T>::invoke(callbacks->valueCallback, value);
                break;

            case SyncReason::ByError:
                Function<E>::invoke(callbacks->errorCallback, value);
                break;

            case SyncReason::ByComplete:
                if (callbacks->completeCallback)
                    callbacks->completeCallback();
                break;

            default:
                Q_ASSERT(false);
            }
        });
    }
};
