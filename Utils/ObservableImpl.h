#pragma once

#include <memory>

#include "SubscriptionProvider.h"

struct Subscription
{
    bool isActive() const { return false; }
    void cancel() {}
};


template <typename T, typename E>
class ObservableImpl
{
    using Subscriber = SubscriptionProvider<T, E>;
    using SubscriberRef = std::weak_ptr<Subscriber>;

    SubscriberRef m_subscriberRef;

public:
    bool m_subscribed = false;
    typename Subscriber::Params m_params;

    ObservableImpl(const ObservableImpl&) = delete;
    ObservableImpl& operator=(const ObservableImpl&) = delete;

    ObservableImpl(ObservableImpl&&) = default;
    ObservableImpl& operator=(ObservableImpl&&) = default;

    explicit ObservableImpl(SubscriberRef&& ref)
        : m_subscriberRef(std::move(ref))
    {}

    Subscription subscribe()
    {
        auto subscriber = m_subscriberRef.lock();

        if (subscriber && !m_subscribed)
        {
            subscriber->subscribe(std::move(m_params));
            m_subscribed = true;

            return {};
        }

        return {};
    }
};
