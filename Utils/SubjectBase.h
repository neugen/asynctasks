#pragma once

#include "SubjectImpl.h"

template <typename T, typename E>
class SubjectBase : public SubjectImpl<T, E>
{
    using Impl = SubjectImpl<T, E>;

public:
    explicit SubjectBase(typename Impl::Type type)
        : Impl(type)
    {}

    void setError(const E& error)
    {
        this->sync(QVariant::fromValue<E>(error), Impl::SyncReason::ByError);
    }
};


template <typename T>
class SubjectBase<T, void> : public SubjectImpl<T, void>
{
    using Impl = SubjectImpl<T, void>;

public:
    explicit SubjectBase(typename Impl::Type type)
        : Impl(type)
    {}

    void setError()
    {
        this->sync({}, Impl::SyncReason::ByError);
    }
};
