#pragma once

#include <functional>
#include <type_traits>

#include <QVariant>

template <typename T>
struct Function
{
    using type = std::function<void(const T&)>;

    static void invoke(const type& callback, QVariant value)
    {
        if (callback)
            callback(value.value<T>());
    }
};

template <>
struct Function<void>
{
    using type = std::function<void()>;

    static void invoke(const type& callback, QVariant)
    {
        if (callback)
            callback();
    }
};
