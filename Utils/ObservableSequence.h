#pragma once

#include "ObservableImpl.h"

template <typename T, typename E>
class ObservableSequence : private ObservableImpl<T, E>
{
    enum BuildFlags
    {
        HasCompleteCallback =  1 << 0,
        HasErrorCallback    =  1 << 1
    };

    using Impl = ObservableImpl<T, E>;
    using Subscriber = SubscriptionProvider<T, E>;
    using ValueCallbackType = typename Subscriber::ValueCallbackType;
    using ErrorCallbackType = typename Subscriber::ErrorCallbackType;

public:
    explicit ObservableSequence(std::weak_ptr<Subscriber>&& ref)
        : Impl(std::move(ref))
    {}

    class Builder
    {
        Impl& m_impl;
        int m_flags = 0;

    public:
        explicit Builder(Impl& impl, int flags)
            : m_impl(impl)
            , m_flags(flags)
        {}

        ~Builder()
        {
            if (!m_impl.m_subscribed)
                m_impl.subscribe();
        }

        Builder onComplete(std::function<void()>&& callback)
        {
            Q_ASSERT(!(m_flags & BuildFlags::HasCompleteCallback) && "multiple onCompleted");
            m_impl.m_params.callbacks->completeCallback = std::move(callback);

            return Builder(m_impl, m_flags | BuildFlags::HasCompleteCallback);
        }

        Builder onError(typename Function<E>::type&& callback)
        {
            Q_ASSERT(!(m_flags & BuildFlags::HasErrorCallback) && "multiple onError");
            m_impl.m_params.callbacks->errorCallback = std::move(callback);

            return Builder(m_impl, m_flags | BuildFlags::HasErrorCallback);
        }

        Subscription getSubscription()
        {
            return m_impl.subscribe();
        }
    };

    Builder onNext(QObject* context, ValueCallbackType&& callback)
    {
        Q_ASSERT(!this->m_subscribed && "already subscribed");

        this->m_params = typename Subscriber::Params(
                    context, std::move(callback));

        return Builder(*this, 0);
    }

    bool isSubscribed() const
    {
        return this->m_subscribed;
    }
};
