#pragma once

#include "SubjectBase.h"
#include "ObservableValue.h"

template <typename T, typename E = void>
class SinglePromise : public SubjectBase<T, E>
{
    using Base = SubjectBase<T, E>;

public:
    using Observable = ObservableValue<T, E>;

    explicit SinglePromise()
        : Base(Base::Type::Single)
    {}

    Observable observable() const
    {
        return Observable(this->subscriber());
    }

    void set(const T& value)
    {
        this->sync(QVariant::fromValue<T>(value), Base::SyncReason::ByValue);
    }
};


template <typename E>
class SinglePromise<void, E> : public SubjectBase<void, E>
{
    using Base = SubjectBase<void, E>;

public:
    using Observable = ObservableValue<void, E>;

    explicit SinglePromise()
        : Base(Base::Type::Single)
    {}

    Observable observable() const
    {
        return Observable(this->subscriber());
    }

    void set()
    {
        this->sync({}, Base::SyncReason::ByValue);
    }
};
