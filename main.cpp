#include <QCoreApplication>

#include "Utils/SinglePromise.h"
#include "Utils/SequencePromise.h"

void testPromise()
{
    QObject ctx;

    SinglePromise<bool> promise;
    auto observable = promise.observable();

    observable.onComplete(&ctx, [](bool)
    {
        Q_UNIMPLEMENTED();

    }).onError([]()
    {
        Q_UNIMPLEMENTED();
    });

    promise.set(true);
    promise.setError();
}

void testObservable()
{
    QObject ctx;

    SequencePromise<int, QString> promise;
    auto observable = promise.observable();

    observable.onNext(&ctx, [](int)
    {
        Q_UNIMPLEMENTED();

    }).onComplete([]()
    {
        Q_UNIMPLEMENTED();

    }).onError([](QString )
    {
        Q_UNIMPLEMENTED();
    });

    promise.setNext(123);
    promise.setError("x");
    promise.setComplete();
}

void testObservableSubscription()
{
    QObject ctx;
    Subscription subscription;

    SequencePromise<int> promise;
    auto observable = promise.observable();

    subscription = observable.onNext(&ctx, [](int)
    {
        Q_UNIMPLEMENTED();

    }).getSubscription();

    if (subscription.isActive())
        subscription.cancel();

    for (int i = 0; i < 3; ++i)
    {
        promise.setNext(i);
    }

    promise.setError();
    promise.setComplete();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    testPromise();
    testObservable();
    testObservableSubscription();

    return a.exec();
}
